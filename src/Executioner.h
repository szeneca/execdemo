#ifndef EXECUTIONER_EXECUTIONER_H
#define EXECUTIONER_EXECUTIONER_H

#include <queue>
#include <string>

struct Task
{
    Task(const std::string& msg, int time)
    : msg_(msg), time_(time)
    {}

    std::string msg_;
    int time_ = {0};
};

class Executioner
{
public:
    void Start();
    void Stop();

    void AddTask(const Task& task);
    void AddInterrupt(const Task& task);

    void Execute();

    bool Active();

private:
    bool active_ = {false};

    std::queue<Task> taskQueue_;
    std::queue<Task> interruptTaskQueue_;
};

#endif