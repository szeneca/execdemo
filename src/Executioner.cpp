#include "Executioner.h"

#include <chrono>
#include <iostream>
#include <thread>

//using TimeUnit = std::chrono::milliseconds;
using TimeUnit = std::chrono::microseconds;

namespace 
{
    std::thread t;

    void ExecutionerThread(Executioner* p)
    {
        while (p->Active())
        {
            p->Execute();
        }
    }
}

void Executioner::Start()
{
    active_ = true;
    t = std::thread(ExecutionerThread, this);
}

void Executioner::Stop()
{
    active_ = false;
    if(t.joinable())
    {
        t.join();
    }
}

bool Executioner::Active()
{
    return active_;
}

void Executioner::AddTask(const Task& task)
{
    taskQueue_.emplace(task);
}

void Executioner::AddInterrupt(const Task& task)
{
    interruptTaskQueue_.emplace(task);
}

void Executioner::Execute()
{
    if (!taskQueue_.empty())
    {
        Task currentTask = taskQueue_.front();

        std::cout << currentTask.msg_ << ": " << currentTask.time_ << std::endl;

        std::chrono::time_point<std::chrono::high_resolution_clock> taskStart = std::chrono::high_resolution_clock::now();
        std::chrono::time_point<std::chrono::high_resolution_clock> taskActive = taskStart;

        // loop condition is artificially easy to interrupt accurately?
        // in reality this would be checking for receipt indicating completion of motion(?)
        while(std::chrono::duration_cast<TimeUnit>(taskActive - taskStart) < TimeUnit(currentTask.time_))
        {
            taskActive = std::chrono::high_resolution_clock::now();
            if(!interruptTaskQueue_.empty())
            {
                Task interrupt = interruptTaskQueue_.front();

                std::cout << interrupt.msg_ << std::endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(10));

                interruptTaskQueue_.pop();

                continue;
            }
            std::this_thread::sleep_for(TimeUnit(100));
        }
        std::chrono::time_point<std::chrono::high_resolution_clock> taskEnd = std::chrono::high_resolution_clock::now();
    
        std::cout << " time = " << std::chrono::duration_cast<TimeUnit>(taskEnd - taskStart).count() << std::endl;

        taskQueue_.pop();
    }
    else
    {
        active_ = false; // stop executing tasks (for demo)
        std::cout << "No tasks left!" << std::endl;
    }
}