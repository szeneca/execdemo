#include "Executioner.h"

#include <thread>
#include <chrono>

//using TimeUnit = std::chrono::milliseconds;
using TimeUnit = std::chrono::microseconds;

int main(int argc, char* argv[])
{
    Executioner myExec;

    myExec.AddTask(Task{"Task 1", 500});
    myExec.AddTask(Task{"Task 2", 1000});
    myExec.AddTask(Task{"Task 3", 2000});
    myExec.AddTask(Task{"Task 4", 350});

    myExec.Start();

    bool sentInterrupt = false;

    while(myExec.Active())
    {
        std::this_thread::sleep_for(TimeUnit(2000));
        if (!sentInterrupt)
        {
            myExec.AddInterrupt(Task{"INTERRUPTED!", 0});
            sentInterrupt = true;
        }
    }

    myExec.Stop();

    return 0;
}